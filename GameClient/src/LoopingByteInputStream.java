import java.io.ByteArrayInputStream;
import java.io.IOException;

public class LoopingByteInputStream extends ByteArrayInputStream {

    private boolean closed;
    public LoopingByteInputStream(byte[] buffer) {
        super(buffer);
        closed = false;
    }
    public int read(byte[] buffer, int offset, int length) {
        if (closed) {
            return -1;
        }
        int totalBytesRead = 0;

        while (totalBytesRead < length) {
            int numBytesRead = super.read(buffer,
                offset + totalBytesRead,
                length - totalBytesRead);

            if (numBytesRead > 0) {
                totalBytesRead += numBytesRead;
            }
            else {
                reset();
            }
        }
        return totalBytesRead;
    }


    /**
        Closes the stream. Future calls to the read() methods
        will return 1.
    */
    public void close() throws IOException {
        super.close();
        closed = true;
    }

}

